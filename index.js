require('dotenv').config()

const express = require('express')
const app = express()

var tool = require('./function')


app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/sum', (req, res) => {
    let sum = tool.calculateSum([1,2,3])
    res.send(
        `The sum is: ${sum.toString()}`
    )
})

app.listen(process.env.PORT || 5000, () => {
  console.log(`Example app listening at http://localhost:${process.env.PORT}`)
})