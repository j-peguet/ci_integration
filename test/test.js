var tool = require('../function')

var assert = require('assert');
describe('Array', function () {
  describe('#calculateSum()', function () {
    it('should the sum of the Array element, 6', function () {
      assert.strictEqual(tool.calculateSum([1, 2, 3]), 6);
    });
  });
});