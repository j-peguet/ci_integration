module.exports = {
    calculateSum: function (array) {
        return array.reduce((a, b) => a + b, 0)
    }
};